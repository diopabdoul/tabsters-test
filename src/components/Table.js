import React from 'react';

const Table = ({datatable}) => {
    return (
        <div>
            <table class="table table-bordered table-hover">
                    <th scope="col">Name</th>
                    <th scope="col">Status</th>
                    <th scope="col">Progress</th>
                    {datatable 
                    .map(item => (
                        <tr key={item.id}>
                            <td >{item.name}</td>
                            <td>{item.status}</td> 
                            <td>{item.progress}</td> 
                        </tr>
                    ))}
                 </table>
            
        </div>
    );
};

export default Table;