
import React, {useState} from 'react';
import TasksData from '../data/tasks.json'
import Table from './Table';

const Affichage = () => {

    const [datatable, setDatatable] = useState(TasksData);


    const sort = () => {
        const sortedData = TasksData.sort((a,b) => a.name.localeCompare(b.name))
        setDatatable(sortedData)
    }

    const moyenne = () => {
        var sum = 0
        TasksData.forEach(a => sum += a.progress)
        const moyenne = sum /TasksData.length
        alert(moyenne)
    }

    const filterToDo = () => {
        const filterTodoData = TasksData.filter(a => a.status === "To Do")
        setDatatable(filterTodoData)
    }

    const filterInProgress = () => {
        const filterInProgressData = TasksData.filter(a => a.status === "In Progress")
        setDatatable(filterInProgressData)
    }

    const filterCompleted = () => {
        const filterCompletedData = TasksData.filter(a => a.status === "Completed")
        setDatatable(filterCompletedData)
    }

    return (
        <div className="affichage">

            <Table datatable={datatable}/>
            <button type="button" class="btn btn-outline-primary" onClick={sort}>Trier</button>
            <button type="button" class="btn btn-outline-secondary " onClick={filterToDo}>To Do</button>
            <button type="button" class="btn btn-outline-secondary " onClick={filterInProgress}>In Progress</button>
            <button type="button" class="btn btn-outline-secondary " onClick={filterCompleted}>Completed</button>
            <button type="button" class="btn btn-outline-primary " onClick={moyenne}>Moyenne</button>
        </div>
    );
};

export default Affichage;